angular.module('ExampleApp', ['ngDraggable']).
    controller('MainCtrl', function ($scope, $timeout) {

        $scope.totalDesk = 4;
        $scope.employee = [];
        $scope.deskRecord = [];

        $scope.employee = [
            {
                deskNo:0,
                seatNo: 0,
                project: 'xyz',
                name: 'Mr. X1',
                employeeId:1
            },{
                deskNo:0,
                seatNo: 1,
                project: 'xyz',
                name: 'Mr. X2',
                employeeId:2
            },{
                deskNo:0,
                seatNo: 2,
                project: 'xyz',
                name: 'Mr. X2',
                employeeId:3
            },{
                deskNo:0,
                seatNo: 3,
                project: 'xyz',
                name: 'Mr. X3',
                employeeId:4
            },{
                deskNo:1,
                seatNo: 0,
                project: 'xyz',
                name: 'Mr. X4',
                employeeId:5
            },{
                deskNo:1,
                seatNo: 1,
                project: 'abc',
                name: 'Mr. X5',
                employeeId:6
            },{
                deskNo:1,
                seatNo: 2,
                project: 'abc',
                name: 'Mr. X1',
                employeeId:7
            },{
                deskNo:1,
                seatNo: 3,
                project: 'pqr',
                name: 'Mr. X6',
                employeeId:7
            },{
                deskNo:2,
                seatNo: 0,
                project: 'pqr',
                name: 'Mr. X7',
                employeeId:8
            },{
                deskNo:2,
                seatNo: 0,
                project: 'pqr',
                name: 'Mr. X8',
                employeeId:9
            }
        ];

        $scope.newEmployee = [
            {
                deskNo:-1,
                seatNo: -1,
                project: '',
                name: 'Mr. y1',
                employeeId:10
            },{
                deskNo:-1,
                seatNo: -1,
                project: '',
                name: 'Mr. y2',
                employeeId:11
            },{
                deskNo:-1,
                seatNo: -1,
                project: '',
                name: 'Mr. y3',
                employeeId:12
            }
        ];
        $scope.resignedEmployee = [
            {
                deskNo:-1,
                seatNo: -1,
                project: '',
                name: 'Mr. Nobody',
                employeeId:0
            }
        ];

        for(var j=0;j< $scope.totalDesk; j++){
            $scope.deskRecord.push(j);
        }

        for(var i = $scope.employee.length ; i < $scope.totalDesk*4; i++){
            var obj = {
                deskNo:-1,
                seatNo : -1,
                project : 'xyz',
                name:''
            }
            $scope.employee.push(obj);
        }

        $scope.centerAnchor = true;

        $scope.titleContent = function(e){
            return '<div><span><label>Name : </label>' +e.name + '</span></br>'+
                '<span><label>Project : </label>' + e.project + '</span></br>'+
                '<span><label>Employee Id : </label>' + e.employeeId + '</span></div>';
        }

        $('body').tooltip({
            selector: '[data-toggle=tooltip]',
            html:true,
            placement:"right"
        });


        $scope.onDropComplete=function(data, index, seat){
            if(!data) return;

            var deskNo = data.deskNo;
            var seatNo = data.seatNo;
            var project = data.project;
            var name = data.name;
            var employeeId = data.employeeId;

            $scope.employee[4*index+ seat].deskNo = index;
            $scope.employee[4*index+ seat].seatNo = seat;
            $scope.employee[4*index+ seat].project = project;
            $scope.employee[4*index+ seat].name = name;
            $scope.employee[4*index+ seat].employeeId = employeeId;

            if (deskNo != -1 && seatNo != -1) {
                $scope.employee[4*deskNo+ seatNo].deskNo = -1;
                $scope.employee[4*deskNo+ seatNo].seatNo = -1;
            }

            if(deskNo == -1 && seatNo == -1){

                var idx = $scope.newEmployee.reduce( function( cur, val, index ){

                    if( val.employeeId === employeeId && cur === -1 ) {
                        return index;
                    }
                    return cur;

                }, -1 );

                if (idx > -1) {
                    $scope.newEmployee.splice(idx, 1);
                }
            }
        }

        $scope.onDropComplete1=function(data,evt){
            debugger;

            if(data.deskNo == -1 && data.seatNo == -1) return;

            var idx = $scope.employee.reduce( function( cur, val, index ){

                if( val.employeeId === data.employeeId && cur === -1 ) {
                    return index;
                }
                return cur;

            }, -1 );

            if (idx > -1) {
                data.seatNo = -1;
                data.deskNo = -1;
                data.employeeId = 0;
                data.project = '';
                $scope.employee.splice(idx, 1);
            }
            $scope.resignedEmployee.push(data);

            console.log($scope.employee);
            console.log($scope.resignedEmployee);

            return;
        }

    });